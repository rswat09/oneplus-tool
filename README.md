# oneplus-tool

This is a tool for rooting and managing your OnePlus device.

**THIS IS STILL IN HEAVY DEVELOPMENT.**

## Prerequisites

* A Linux PC (a Windows PC with WSL might work too).
* ADB and Fastboot installed (`adb` and `fastboot` on Ubuntu)
* A OnePlus device booted and in Android (not the recovery or bootloader) with OEM unlocking and USB debugging enabled in settings.

## How to use

You can run these commands:

```sh
git clone https://gitlab.com/rswat09/oneplus-tool
cd oneplus-tool && chmod +x run
./run
```

## Devices supported

* `denniz` (OnePlus Nord 2 5G): support in https://t.me/OnePlusNord2GlobalOfficial