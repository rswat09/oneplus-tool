#!/bin/bash

wait_for_device() {
  echo "waiting for device to be connected..."

  zenity --info --width 200 --title "Waiting" --text="Waiting for you to plug in your phone..." &
  zpid=$!
  adb wait-for-device
  kill $zpid
}

set -e

DEVICES=()
cd devices/ && for device in *; do
  DEVICES+=("$device")
done
cd ..

DEVICE="$(zenity --list \
  --title="Choose a OnePlus device" \
  --column="Device" \
  "${DEVICES[@]}")"

[[ -d "devices/${DEVICE}" ]] || exit 1

ACTIONS=('Install TWRP' 'Boot to the bootloader' 'Boot to the recovery' 'Upgrade to the latest OxygenOS beta')

ACTION="$(zenity --list \
  --title="Select an action" \
  --text="You have selected the $(cat devices/${DEVICE}/name). What do you want to do? This tool expects that your phone is powered on and is in Android, and USB debugging and OEM unlocking are enabled." \
  --column="Action" \
  "${ACTIONS[@]}")"

case "$ACTION" in
  'Install TWRP')
    wait_for_device
    zenity --info --title "Waiting" --text="Installing TWRP..." &
    zpid=$!
    cd "devices/${DEVICE}"
    adb reboot bootloader
    ( fastboot getvar unlocked 2>&1 | grep -q '^unlocked: yes$' ) || fastboot flashing unlock
    zenity --question --width 200 --text 'Have you installed TWRP on this phone earlier, or disabled verification/verity with fastboot?' \
     || fastboot --disable-verity --disable-verification flash vbmeta vbmeta.img
    fastboot flash recovery twrp.img
    fastboot reboot recovery
    kill $zpid
    zenity --info --title 'Status' --text 'Installed TWRP.'
    ;;
  'Boot to the bootloader')
    wait_for_device
    adb reboot bootloader
    ;;
  'Boot to the recovery')
    wait_for_device
    adb reboot recovery
    ;;
  'Upgrade to the latest OxygenOS beta')
    zenity --info --title 'Warning' --width=400 --text="Upgrading to the latest OxygenOS beta is not recommended as you won't be able to go into the bootloader or root the phone after updating and you'll need to downgrade to the stable version to use this tool again (although downgrading is really easy, so you could try the beta and then return to the stable version by following the instructions on OnePlus's website)"
    ;;
  *)
    exit 1
    ;;
esac